﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Client;
using System.Configuration;
using Microsoft.Xrm.Sdk.Client;

namespace XRM.Service
{
    public class ConnectionService
    {
        public static IOrganizationService GetService()
        {
            string connectionUrl = ConfigurationManager.ConnectionStrings["XRM"].ToString();
            CrmConnection connection = CrmConnection.Parse(connectionUrl);

            var result = new OrganizationServiceProxy(connection.ServiceUri, connection.HomeRealmUri,
                connection.ClientCredentials, connection.DeviceCredentials);

            result.EnableProxyTypes();

            return result;
        }
    }
}